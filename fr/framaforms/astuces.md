# Astuces

## Je souhaite envoyer des mails en fonction des réponses

Je veux envoyer un mail à une adresse différente en fonction du ou des comités séléctioné :
* je vais dans l'onglet **Courriels**
* je sélectionne le composant
* j'ajoute les adresses mail en fonction du comité
* je sauvegarde (tout en bas)

![composant selections multiples](images/astuces_forms_courriel_comite.png)
![courriel en fonction du composant](images/astuces_forms_courriel_comite2.png)

## Je souhaite un système de CAPTCHA

* j'ajoute un champ texte à mon formulaire (en cochant **Requis** dans l'onglet **Validation** du composant)
* dans l'onglet **Validation du formulaire** j'ajoute une règle **Motif**
* je remplis les champs comme sur l'image ci-dessous

![Règle Motif Framaforms](images/astuces_forms_captcha.png)

## Je souhaite un champ « autre » libre

Je voudrais qu'en cliquant sur le choix « autre », les répondant⋅e⋅s puissent mettre un choix libre. Pour cela :

* je créé un composant « Boutons radio » ![composant bouton radio](images/astuces_forms_autre1.png)
* je créé un composant « Champ texte » (ici, avec le titre « Autre ?») ![composant texte autre](images/astuces_forms_autre2.png)
* j'enregistre
* je vais dans « Champs conditionnels »
* je créé la règle : « Si la mascotte est autre alors Autre ? est affiché » (si le champ autre du composant « Boutons radio » est « autre » alors on affiche le composant « Champ texte » - sinon, il ne le sera pas) ![conditions](images/astuces_forms_autre3.png)

Résultats :

![Résultat autre caché](images/astuces_forms_autre4.png)
![Résultat autre visible](images/astuces_forms_autre5.png)
