# Framasphère

Framasphère is a Free, Libre and open source social media network, built over Diaspora\* software. join your friends on Framasphère\*, or any other Diaspora* pod — and begin to share without being tracked.

## Getting started in diaspora

* [1 - Signing up](1-sign_up.html)
* [2 - The interface](2-interface.html)
* [2b - The mobile interface](2b-interface-mobile.html)
* [3 - Aspects](3-aspects.html)
* [4 - Finding and connecting with people](4-connecting.html)
* [5 - Start sharing!](5-sharing.html)
* [6 - Notifications and conversations](6-conversations.html)
* [7 - Finishing up](7-finishing.html)
