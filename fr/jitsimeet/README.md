# Framatalk

[Framatalk](https://framatalk.org) est un service en ligne libre qui permet de créer un salon de visio-conférence et d’y inviter son, sa, ses interlocuteur·trice·s.

Propulsé par le logiciel libre [Jitsi Meet](https://jitsi.org/Projects/JitsiMeet), Framatalk offre de nombreuses options&nbsp;:

* Un tchat pour discuter en mode texte (il vous faudra entrer un pseudo)&nbsp;;
* Un bouton d’invitation à la conversation (partage par email de l’adresse web du salon)&nbsp;;
* Des boutons pour activer/désactiver le micro, la caméra, le mode plein écran&nbsp;;
* Un accès aux paramètres (modifier son pseudo, sa caméra, son micro)&nbsp;;
* La possibilité que Framatalk retienne votre profil de paramètres (il créera un cookie)&nbsp;;
* Les droits de modération du salon (pour la première personne arrivée)&nbsp;;
* La possibilité de protéger le salon par mot de passe (pour le modérateur).

## Créer une discussion en 4 étapes

1. Choisissez le nom de vote salon ![nom du salon](images/framatalk01.png)
2. Autorisez l’utilisation de votre micro ![autorisation micro](images/framatalk02.png)
3. Autorisez l’utilisation de votre caméra ![autorisation caméra](images/framatalk03.png)
4. Partagez l’adresse web (URL) pour converser avec vos ami·e·s ![salon Framatalk](images/framatalk04-1.png)


### Pour aller plus loin&nbsp;:

* [Essayer Framatalk](https://framatalk.org)&nbsp;;
* Le [site officiel de Jitsi meet](https://jitsi.org/Projects/JitsiMeet), moteur de Framatalk&nbsp;;
* Participer au [code de Jitsi Meet](https://github.com/jitsi/jitsi-meet)&nbsp;;
* [Installer Jitsi Meet](http://framacloud.org/cultiver-son-jardin/installation-de-jitsi-meet/) sur vos serveurs&nbsp;;
* [Dégooglisons Internet](https://degooglisons-internet.org)&nbsp;;
* [Soutenir Framasoft](https://soutenir.framasoft.org).

