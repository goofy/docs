# Summary

* [Accueil](README.md)

## Guides


* [Libertés numériques](manueldumo/README.md)
    * [1 — De quels outils ai-je besoin ?](manueldumo/chapitre-1.md)
    * [2 — Le web et les contenus](manueldumo/chapitre-2.md)
    * [3 — Mes messages sur Internet](manueldumo/chapitre-3.md)
    * [4 — Réseaux sociaux et hébergement](manueldumo/chapitre-4.md)
    * [5 — Suis-je en sécurité sur Internet ?](manueldumo/chapitre-5.md)
    * [Conclusion](manueldumo/conclusion.md)
    * [Glossaire](manueldumo/glossaire.md)

* [L'auto-hébergement facile](https://framacloud.org/fr/auto-hebergement/)

* [Comprendre le numérique (avec les doigts)](comprendre/README.md)
    * [Connexion sécurisée et VPN (avec des suppositoires)](comprendre/https-vpn.md)
    * [Pages et navigateurs web (avec TopChef)](comprendre/navigateur-web.md)

## Services libres

* [Framagenda / Nextcloud](nextcloud/README.md)

* [Framabin / PrivateBin](privatebin/README.md)

* [Framabag / Wallabag](wallabag/README.md)
    * [Se créer un compte](wallabag/user/create_account.md)
    * [Configuration](wallabag/user/configuration.md)
    * [Migrer depuis…](wallabag/user/import.md)
    * [Articles](wallabag/user/articles.md)
    * [Erreurs durant la récupération des articles](wallabag/user/errors_during_fetching.md)
    * [Retrouver des articles grâce aux filtres](wallabag/user/filters.md)
    * [Configurer les applications mobile pour wallabag](wallabag/user/configuring_mobile.md)
    * [Application Android](wallabag/user/android.md)

* [Framaboard / Kanboard](kanboard/README.md)
    ### Introduction

    * [Qu'est-ce que Kanban ?](kanboard/what-is-kanban.md)
    * [Comparons Kanban aux Todo listes et à Scrum](kanboard/kanban-vs-todo-and-scrum.md)
    * [Exemples d'utilisation](kanboard/usage-examples.md)

    ### Utiliser un tableau

    * [Vues Tableau, Agenda et Liste](kanboard/project-views.md)
    * [Mode Replié et Déplié](kanboard/board-collapsed-expanded.md)
    * [Défilement horizontal et mode compact](kanboard/board-horizontal-scrolling-and-compact-view.md)
    * [Afficher ou cacher des colonnes dans le tableau](kanboard/board-show-hide-columns.md)

    ### Travailler avec les projets

    * [Types de projets](kanboard/project-types.md)
    * [Créer des projets](kanboard/creating-projects.md)
    * [Modifier des projets](kanboard/editing-projects.md)
    * [Supprimer des projets](kanboard/removing-projects.md)
    * [Partager des tableaux et des tâches](kanboard/sharing-projects.md)
    * [Actions automatiques](kanboard/automatic-actions.md)
    * [Permissions des projets](kanboard/project-permissions.md)
    * [Swimlanes](kanboard/swimlanes.md)
    * [Calendriers](kanboard/calendar.md)
    * [Analytique](kanboard/analytics.md)
    * [Diagramme de Gantt pour les tâches](kanboard/gantt-chart-tasks.md)
    * [Diagramme de Gantt pour tous les projets](kanboard/gantt-chart-projects.md)
    * [Rôles personnalisés pour les projets](kanboard/custom-project-roles.md)

    ### Travailler avec les tâches

    * [Créer des tâches](kanboard/creating-tasks.md)
    * [Fermer des tâches](kanboard/closing-tasks.md)
    * [Dupliquer et déplacer des tâches](kanboard/duplicate-move-tasks.md)
    * [Ajouter des captures d'écran](kanboard/screenshots.md)
    * [Liens internes entre les tâches](kanboard/task-links.md)
    * [Transitions](kanboard/transitions.md)
    * [Suivi du temps](kanboard/time-tracking.md)
    * [Tâches récurrentes](kanboard/recurring-tasks.md)
    * [Créer des tâches par email](kanboard/create-tasks-by-email.md)
    * [Sous-tâches](kanboard/subtasks.md)
    * [Analytique des tâches](kanboard/analytics-tasks.md)
    * [Mentionner les utilisateurs](kanboard/user-mentions.md)

    ### Travailler avec les utilisateurs

    * [Rôles](kanboard/roles.md)
    * [Gestion des utilisateurs](kanboard/user-management.md)
    * [Notifications](kanboard/notifications.md)
    * [Authentification à deux facteurs](kanboard/2fa.md)

    ### Paramètres

    * [Raccourcis clavier](kanboard/keyboard-shortcuts.md)
    * [Paramètres de l'application](kanboard/application-configuration.md)
    * [Paramètres du projet](kanboard/project-configuration.md)
    * [Paramètres du tableau](kanboard/board-configuration.md)
    * [Paramètres du calendrier](kanboard/calendar-configuration.md)
    * [Paramètres du lien](kanboard/link-labels.md)
    * [Taux de change](kanboard/currency-rate.md)

* [Framacalc / Ethercalc](ethercalc/README.md)
    * [Fonctionnalités](ethercalc/fonctionnalites.md)

* [Framacarte / uMap](umap/README.md)
    ### Niveau débutant

    - [1 - Je consulte une carte umap](umap/1-consulter.md)
    - [2 - Je crée ma première carte umap](umap/2-premiere-carte.md)
    - [3 - J'utilise un compte et crée une belle carte](umap/3-utiliser-un-compte.md)
    - [4 - Je modifie et personnalise ma carte](umap/4-personnaliser.md)

    ### Niveau intermédiaire

    - [5 - Je crée des infobulles multimédia](umap/5-infobulles-multimedia.md)
    - [6 - Je structure ma carte avec des calques](umap/6-calques.md)
    - [7 - Je publie ma carte et en contrôle l'accès](umap/7-publication-et-droit-d-acces.md)
    - [8 - Le cas des polygones](umap/8-polygones.md)

    ### Niveau avancé

    - [9 - Je crée une carte à partir d'un tableur](umap/9-a-partir-d-un-tableur.md)
    - [11 - Je valorise les données OpenStreetMap avec uMap](umap/11-valoriser-OpenStreetMap.md)

* [Framaclic / Dolomon](dolomon/README.md)
    * [Exemple d'utilisation](dolomon/exemple-d-utilisation.md)

* [Framadate](framadate/README.md)
    * [Prise en main](framadate/prise-en-main.md)

* [Framadrive / Nextcloud](nextcloud/README.md)
    * [Exemple d'utilisation Framagenda](nextcloud/exemple-d-utilisation.md)
    * [Inscription et Connexion](nextcloud/Inscription-Connexion.md)
    * [Agenda](nextcloud/Interface-Agenda.md)
    * [Contacts](nextcloud/Interface-Contacts.md)
    * [Tâches](nextcloud/Interface-Tasks.md)
    * [FAQ](nextcloud/FAQ.md)

    ### Synchronisation

    * [Android](nextcloud/Synchronisation/Android.md)
    * [iOS](nextcloud/Synchronisation/iOS.md)
    * [Gnome](nextcloud/Synchronisation/gnome.md)
    * [Windows Phone](nextcloud/Synchronisation/WindowsPhone.md)
    * [Ubuntu Touch](nextcloud/Synchronisation/UbuntuTouch.md)
    * [MacOS Calendier](nextcloud/Synchronisation/ical.md)
    * [MacOS Contacts](nextcloud/Synchronisation/icard.md)
    * [Thunderbird](nextcloud/Synchronisation/Thunderbird.md)

* [Framadrop / Lufi](lufi/README.md)

* [Framaforms](framaforms/README.md)
    * [Exemple d'utilisation](framaforms/exemple-d-utilisation.md)
    * [Astuces](framaforms/astuces.md)
    * [Fonctionnalités](framaforms/fonctionnalites.md)
    * [Les composants](framaforms/composants.md)

* [Framagit / Gitlab](gitlab/README.md)
    * [1 - Quelques mots d'introduction sur Git](gitlab/1-introduction.md)
    * [2 - La création et la configuration de votre compte sur Framagit](gitlab/2-creation-configuration-compte.md)
    * [3 - Installation de votre poste de travail](gitlab/3-installation-poste.md)
    * [4 - Comment gérer la communication au sein des membres de l'équipe](gitlab/4-gerer-communication.md)
    * [5 - Activités sur le projet](gitlab/5-activites-projet.md)
    * [Comment utiliser les Gitlab Pages ?](gitlab/gitlab-pages.md)

* [Framalink / Lstu](lstu/README.md)
    * [Fonctionnalités](lstu/fonctionnalites.md)

* [Framalistes / Sympa](sympa/README.md)
    * [Exemple d'utilisation](sympa/exemple-d-utilisation.md)

* [Framaestro](framaestro/README.md)
    * [Exemple d'utilisation](framaestro/exemple-d-utilisation.md)

* [Framanotes / Turtl](turtl/README.md)
    * [Exemple d'utilisation](turtl/exemple-d-utilisation.md)

* [Framapiaf / Mastodon](mastodon/README.md)
    * [Framapiaf en 5min](mastodon/5min.md)
    * [Premiers pas](mastodon/User-guide.md#premiers-pas)
    * [Confidentialité, sûreté et sécurité](mastodon/User-guide.md#confidentialité-sûreté-et-sécurité)
    * [Applications](mastodon/Apps.md)

* [Framapic / Lutim](lutim/README.md)
    * [Faire une galerie photo](lutim/galerie.md)

* [Framapad / Etherpad](etherpad/README.md)
    * [Extensions](etherpad/extensions.md)

* [Framasite / Dokuwiki](dokuwiki/README.md)
    * [Astuces](dokuwiki/astuces.md)

* [Framasite / Grav](grav/README.md)
    * [Exemple d'utilisation](grav/exemple-d-utilisation.md)
    * [Prise en main](grav/prise-en-main.md)
    * [Composants de base](grav/composants-de-base.md)
    * [Composants Bootstrap](grav/composants-bootstrap.md)
    * [Modules](grav/modules.md)
    * [Markdown](grav/markdown.md)

* [Framaslides / Strut](strut/README.md)
    * [Exemple d'utilisation](strut/exemple-d-utilisation.md)
    * [Créer sa première présentation](strut/create_first_presentation.md)
    * [Images](strut/pictures.md)
    * [Vidéos](strut/videos.md)
    * [Arrière plan](strut/background.md)
    * [Raccourcis clavier](strut/shortcuts.md)
    * [Groupes](strut/groups.md)

* [Framasphère / Diaspora*](diaspora/README.md)
  ### Wiki
    * [1 - Connexion](diaspora/1-connexion.md)
    * [2 - Interface](diaspora/2-interface.md)
    * [2b - Interface mobile](diaspora/2b-interface-mobile.md)
    * [3 - Aspects](diaspora/3-aspects.md)
    * [4 - Échange](diaspora/4-echange.md)
    * [5 - Partage](diaspora/5-partage.md)
    * [6 - Notification et conversation](diaspora/6-notification-conversation.md)
    * [7 - Pour terminer](diaspora/7-fin.md)
    * [Kit de base](diaspora/kit-de-base.md)

* [Framatalk / Jitsi Meet](jitsimeet/README.md)

* [Framateam / Mattermost](mattermost/README.md)
    * [Exemple d'utilisation](mattermost/exemple-d-utilisation.md)

  ### Premiers pas

    * [Identification](mattermost/help/getting-started/signing-in.md)
    * [Bases de la messagerie](mattermost/help/getting-started/messaging-basics.md)
    * [Configuration des notifications](mattermost/help/getting-started/configuring-notifications.md)
    * [Organisation des conversations](mattermost/help/getting-started/organizing-conversations.md)
    * [Recherche](mattermost/help/getting-started/searching.md)
    * [Création d’équipes](mattermost/help/getting-started/creating-teams.md)
    * [Gestion des membres](mattermost/help/getting-started/managing-members.md)

  ### Messagerie

    * [Envoi de messages](mattermost/help/messaging/sending-messages.md)
    * [Mentionner des correspondants/amis](mattermost/help/messaging/mentioning-teammates.md)
    * [Formater le texte](mattermost/help/messaging/formatting-text.md)
    * [Joindre des fichiers](mattermost/help/messaging/attaching-files.md)
    * [Exécuter des commandes](mattermost/help/messaging/executing-commands.md)

  ### Paramètres

    * [Paramètres du compte](mattermost/help/settings/account-settings.md)
    * [Couleurs du thème](mattermost/help/settings/theme-colors.md)
    * [Paramètres du canal](mattermost/help/settings/channel-settings.md)
    * [Paramètres d’équipe](mattermost/help/settings/team-settings.md)

* [Framavox / Loomio](loomio/README.md)
    * [Exemple d'utilisation](loomio/exemple-d-utilisation.md)
    * [Les types de votes](loomio/types-votes.md)
    * [Avant de commencer](loomio/getting_started.md)
    * [Réglages de groupe](loomio/group_settings.md)
    * [Coordonner votre groupe](loomio/coordinating_your_group.md)
    * [Inviter de nouveaux membres](loomio/inviting_new_members.md)
    * [Discussion](loomio/discussion_threads.md)
    * [Commentaires](loomio/comments.md)
    * [Propositions](loomio/proposals.md)
    * [Sous-groupes](loomio/subgroups.md)
    * [Naviguer dans Loomio](loomio/reading_loomio.md)
    * [Rester à jour](loomio/keeping_up_to_date.md)
    * [Votre profil](loomio/your_user_profile.md)

* [MyFrama / Shaarli](shaarli/README.md)
    * [Exemple d'utilisation](shaarli/exemple-d-utilisation.md)

## Culture et logiciels libres

* [Framakey](framakey/README.md)
    * [Installation](framakey/installer.md)
    * [Prise en main](framakey/utiliser.md)
    * [Ajouter des applications](framakey/ajouter-apps.md)

* [Framalibre](framalibre/README.md)
    * [Premiers pas sur Framalibre](framalibre/premiers-pas.md)
    * [De bonnes raisons pour se créer un compte](framalibre/se-creer-un-compte.md)
    * [Comment créer ou modifier une notice ?](framalibre/creer-modifier-une-notice.md)

* [Framapack](framapack/README.md)
