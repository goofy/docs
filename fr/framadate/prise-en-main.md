# Planifier un rendez-vous rapidement avec Framadate

<p class="h4">
    Un service libre respectueux des données privées,
    fourni par <b class="violet">Frama</b><b class="orange">soft</b>
</p>

## 0. Ouverture

![](img/1.png)

Pour créer un nouveau sondage, rendez-vous sur <https://framadate.org>.

<p>Puis cliquez sur
<button class="btn btn-primary">
    <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
    Créer un sondage spécial dates
</button></p>

Notez que vous pourrez retrouver tous vos sondages en cliquant sur
«&nbsp;Où sont mes sondages&nbsp;».
Il suffira alors de renseigner votre adresse courriel.

---

## 1. Configuration

![](img/2.png)

Remplissez le formulaire de présentation du sondage,

Votre adresse courriel ne sera utilisée que pour communiquer les
informations relatives au sondage (conformément à
[la charte des services Framasoft](https://framasoft.org/nav/html/charte.html)).

**Cases à cocher&nbsp;:** choisissez parmi les options proposées.
*Astuce&nbsp;: recevoir un  courriel à chaque fois qu'un sondé remplit
une ligne est utile pour suivre l'avancée du sondage.*

![](img/3.png)

Définissez les dates du sondage en utilisant le menu contextuel
affichant un calendrier, puis affinez vos dates en entrant des horaires.
Les boutons + et – vous permettent d'ajouter autant de champs que nécessaire.

<p class="alert alert-info">
    Notez le bouton suivant
    <button class="btn btn-default"
        title="Reporter les horaires du premier jour sur les autres jours">
        <i class="glyphicon glyphicon-sort-by-attributes-alt text-info" aria-hidden="true"></i>
        <span class="sr-only">Reporter les horaires du premier jour sur les autres jours</span>
    </button>
    qui, une fois définis les horaires pour une première date,
    vous permet de cloner ces horaires, si besoin.
</p>

![](img/5.png)

Dans le récapitulatif de votre sondage, vous pouvez préciser vous-même
une date de suppression automatique du sondage.

Vous pouvez alors **valider** la création de votre sondage

---

## 2. Administration

![](img/6.png)

Après la création du sondage, vous accédez directement à son interface
d'administration.
Ces données vous sont envoyées par courriel avec 2 adresses :

 *  **le lien public du sondage** à faire parvenir à vos
    correspondants,
 *  **le lien d'administration** à conserver pendant
    la durée du sondage.

Si besoin, vous pouvez d’ores et déjà accomplir votre propre
participation au sondage ou indiquer vous-même la participation
d'une personne.

Notez que depuis l'interface d'administration vous pourrez **exporter**
les résultats du sondage en .csv (à ouvrir avec un tableur comme Excel
ou LibreOffice Calc).

<p>Pour cela, cliquez sur le bouton
<button class="btn btn-default">
    <i class="glyphicon glyphicon-download-alt" aria-hidden="true"></i>
    Export Tableur (CSV)
</button></p>

---

## 3. Procédure de votation

![](img/7.png)

 1. Indiquer son nom.
 -  Pour chaque champ, choisir «&nbsp;oui&nbsp;»,
    «&nbsp;si nécessaire&nbsp;» ou «&nbsp;non&nbsp;» (par défaut).
 -  Enregistrer ses choix.
 -  Il est possible de visualiser les résultats du sondage sous forme graphique.
 -  À chaque vote validé, les résultats du sondage sont actualisés.

---

<p class="text-muted text-center">
    Auteur : Christophe Masutti, Framasoft, juin 2015<br>
    Ce tutoriel est placé sous
    <a href="http://artlibre.org/licence/lal/">Licence Art Libre</a>.<br>
    <img src="img/Logo_Licence_Art_Libre.png" alt="" width="20%" />
</p>