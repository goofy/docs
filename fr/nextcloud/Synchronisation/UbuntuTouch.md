# Synchronisation avec Ubuntu Touch
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](../README.md)

Il faut se rendre dans **Paramètres système** puis **Comptes**, **Ajouter un compte**, puis sélectionner **ownCloud**.

Vous devez alors renseigner :
* URL : https://framagenda.org/
* l'identifiant de votre compte Framagenda
* le mot de passe de votre compte Framagenda.


Pour terminer, cliquez sur Continuer.

---

Ubuntu Touch – version UBPorts (paramètres internes)

Il faut se rendre dans **Paramètres système** puis **Comptes**, **Ajouter un compte**, puis sélectionner **Generic CalDAV**.

Vous devez alors renseigner :
* URL : https://framagenda.org/
* l'identifiant de votre compte Framagenda
* le mot de passe de votre compte Framagenda.

Pour terminer, cliquez sur Continuer.
